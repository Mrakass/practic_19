﻿#include <iostream>
#include <string>

using namespace std;

class Animals {
	
public:	
	virtual void voice() {
		setlocale(0, "");
		cout << "животные издают звуки: \n";
	}

};
class Dog:public Animals
{
public:
	void voice(){
		cout << "ГАВ \n";
	}

};
class Cat:public Animals {
public:
	void voice() {
		cout << "МЯУ \n";
	}
};

class Goose :public Animals {
public:
		void voice() {
			cout << "ГА ГА \n";
	}
};


int main() {
	setlocale(0, "");
	Animals* animal[3];
	animal[0] = new Dog;
	animal[1] = new Cat;
	animal[2] = new Goose;
	for (int i = 0; i < 3; i++) {
		animal[i]->voice();
	}
	return 0;
	
	

}